<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'students';

    protected $fillable = ['nisn', 'nis', 'nama', 'kelas', 'alamat', 'no_telepon'];


    public function getClassFormattedAttribute()
    {
        $value = '';

        if($this->kelas == 0 ){
            $value = "X RPL";
        }
        if($this->kelas == 1 ){
            $value = "XI RPL";
        }
        if($this->kelas == 2 ){
            $value = "XII RPL";
        }
        
        return $value;
    }
    
}
