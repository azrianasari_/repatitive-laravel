<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\Siswa;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $nama = "Azriana";
        // return view('siswa', ['nama' => $nama]);

        // Query Builder
        // $data = DB::table('siswa')->get();

        $data = Siswa::get();
        return view('siswa.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('siswa/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Siswa::create([
            'nisn' => $request->nisn,
            'nis' => $request->nis,
            'nama' => $request->nama,
            'kelas' => $request->kelas,
            'alamat' => $request->alamat,
            'no_telepon' => $request->telp,
        ]);

        //arahkan ke halaman siswa
        return redirect('siswa');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //tampilkan data berdasarkan $id data
         $data = Siswa::findOrFail($id);

         //arahkan dan tampilkan data pada halaman edit
         return view('siswa.show', compact('data'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //tampilkan data berdasarkan $id data
        $data = Siswa::findOrFail($id);

        //arahkan dan tampilkan data pada halaman edit
        return view('siswa.edit', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        Siswa::where('id', $id)
                ->update([
                    'nisn' => $request->nisn,
                    'nis' => $request->nis,
                    'nama' => $request->nama,
                    'kelas' => $request->kelas,
                    'alamat' => $request->alamat,
                    'no_telepon' => $request->telp,
                ]);

        return redirect('siswa');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //hapus data berdasarkan $id
        Siswa::destroy($id);

        //arahkan ke halaman siswa
        return redirect('siswa');

    }
}
