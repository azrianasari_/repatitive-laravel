@extends('layouts.main')
@section('title','Detail Data')

@section('content')

<div class="container mt-5">
    <h3>Detail Data</h3>
<form method="POST" action="{{ "/siswa/$data->id" }}">
  @method('put')
  @csrf

        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="nisn">NISN</label>
            <input type="text" class="form-control" name="nisn" id="nisn" value="{{ $data->nisn }}" disabled>

          </div>
          <div class="form-group col-md-6">
            <label for="nis">NIS</label>
            <input type="text" class="form-control" name="nis" id="nis" value="{{ $data->nis }}" disabled>
          </div>
        </div>

        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" name="nama" class="form-control" id="nama" value="{{ $data->nama }}" disabled>
        </div>

        <div class="form-group">
          <label for="kelas">Kelas</label>
          <select name="kelas" id="kelas" class="form-control" disabled>
              <option value="0" @if($data->kelas == 0) {{'selected'}} @endif>X RPL</option>
              <option value="1" @if($data->kelas == 1) {{'selected'}} @endif>XI RPL</option>
              <option value="2" @if($data->kelas == 2) {{'selected'}} @endif>XII RPL</option>
          </select>
        </div>

        <div class="form-group">
          <label for="alamat">Alamat</label>
          <input type="text" name="alamat" class="form-control" id="alamat" value="{{ $data->alamat }}" disabled>
        </div>

        <div class="form-group">
          <label for="telp">Telepon</label>
          <input type="text" name="telp" class="form-control" id="telp" value="{{ $data->no_telepon }}" disabled>
        </div>
      
      </form>
</div>
@endsection