@extends('layouts.main')

@section('title','Siswa')

@section('content')

<div class="container mt-5">
        <h1>Daftar Siswa</h1>
        <a href={{url('siswa/create')}} class="btn btn-primary btn-sm" >Tambah</a>
        
        <table class="table mt-2">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">NISN/NIS</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $dt)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $dt->nisn }} / {{ $dt->nis }}</td>
                        <td>{{ $dt->nama }}</td>
                        <td>{{ $dt->class_formatted }}</td>
                        <td>
                              <a href="{{"siswa/$dt->id/edit"}}"class="btn btn-warning btn-sm" >edit</a>
                              <form action={{"/siswa/$dt->id"}} method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                  <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin?')">Hapus</button>
                              </form>
                              <a href={{"siswa/$dt->id"}} class="btn btn-primary btn-sm" >Detail</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>

</div>
    
@endsection