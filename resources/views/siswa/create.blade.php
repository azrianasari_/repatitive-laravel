@extends('layouts.main')
@section('title','Tambah Data')

@section('content')

<div class="container mt-5">
    <h3>Tambah Data</h3>
<form method="POST" action="{{ route('siswa.store') }}">
    @csrf
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="nisn">NISN</label>
            <input type="text" class="form-control" name="nisn" id="nisn">
          </div>
          <div class="form-group col-md-6">
            <label for="nis">NIS</label>
            <input type="text" class="form-control" name="nis" id="nis">
          </div>
        </div>

        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" name="nama" class="form-control" id="nama">
        </div>

        <div class="form-group">
          <label for="kelas">Kelas</label>
          <select name="kelas" id="kelas" class="form-control">
              <option value="0">X RPL</option>
              <option value="1">XI RPL</option>
              <option value="2">XII RPL</option>
          </select>
        </div>

        <div class="form-group">
          <label for="alamat">Alamat</label>
          <input type="text" name="alamat" class="form-control" id="alamat">
        </div>

        <div class="form-group">
          <label for="telp">Telepon</label>
          <input type="text" name="telp" class="form-control" id="telp">
        </div>
      
        <button type="submit" class="btn btn-primary">Kirim</button>
      </form>
</div>
@endsection