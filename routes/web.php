<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('index');
});

// Route::get('/siswa', function () {
//     return view('siswa');
// });

// Route::get('/siswa', function () {
//     $nama = "Azriana";
//     return view('siswa', ['nama' => $nama]);
// });


// Route::get('/siswa', 'SiswaController@index');

Route::resource('siswa', 'SiswaController');


Route::get('/bayar', function () {
    return view('pembayaran');
});